{
  pkgs ? import <nixpkgs> {},
  listenAddress ? "0.0.0.0",
  listenPort ? 3000,
  theiaideAppFolder ? "_theiaideApp",
  projectFolder ? "$PWD",
  workingFolder ? "$PWD",
  browser ? "firefox"
}:

let
  theiaideBuildDependencies = [
    pkgs.nano
    pkgs.curl
    pkgs.yarn
    pkgs.nodejs-10_x
    pkgs.git
    pkgs.wget
    pkgs.lzma
    pkgs.gcc
    pkgs.gnumake
    pkgs.gccStdenv
    pkgs.binutils
    pkgs.gnupg
    pkgs.getopt
  ];

  theiaideOpenDependencies = [
    pkgs.firefox
  ];

  projectDependencies = [
    pkgs.python38Full
    pkgs.python38Packages.pip
    pkgs.python38Packages.setuptools
    pkgs.python38Packages.pylint
  ];

  theiaidePackageConfig = {
     private= true;
     theia.frontend.config.applicationName = "NixIde-Package";
     dependencies = {
      "@theia/callhierarchy"= "latest";
      "@theia/console"= "latest";
      "@theia/core"= "latest";
      "@theia/editor"= "latest";
      "@theia/editor-preview"= "latest";
      "@theia/file-search"= "latest";
      "@theia/filesystem"= "latest";
      "@theia/getting-started"= "latest";
      "@theia/git"= "latest";
      "@theia/keymaps"= "latest";
      "@theia/markers"= "latest";
      "@theia/messages"= "latest";
      "@theia/metrics"= "latest";
      "@theia/mini-browser"= "latest";
      "@theia/monaco"= "latest";
      "@theia/navigator"= "latest";
      "@theia/outline-view"= "latest";
      "@theia/output"= "latest";
      "@theia/plugin"= "latest";
      "@theia/plugin-ext"= "latest";
      "@theia/plugin-ext-vscode"= "latest";
      "@theia/preferences"= "latest";
      "@theia/preview"= "latest";
      "@theia/process"= "latest";
      "@theia/scm"= "latest";
      "@theia/search-in-workspace"= "latest";
      "@theia/task"= "latest";
      "@theia/terminal"= "latest";
      "@theia/typehierarchy"= "latest";
      "@theia/userstorage"= "latest";
      "@theia/variable-resolver"= "latest";
      "@theia/vsx-registry"= "latest";
      "@theia/workspace"= "latest";
     };
     theiaPlugins = {
      "vscode-builtin-configuration-editing"= "https://github.com/theia-ide/vscode-builtin-extensions/releases/download/v1.39.1-prel/configuration-editing-1.39.1-prel.vsix";
      "vscode-builtin-handlebars"= "https://github.com/theia-ide/vscode-builtin-extensions/releases/download/v1.39.1-prel/handlebars-1.39.1-prel.vsix";
      "vscode-builtin-json"= "https://github.com/theia-ide/vscode-builtin-extensions/releases/download/v1.39.1-prel/json-1.39.1-prel.vsix";
      "vscode-builtin-json-language-features"= "https://open-vsx.org/api/vscode/json-language-features/1.46.1/file/vscode.json-language-features-1.46.1.vsix";
      "vscode-builtin-log"= "https://github.com/theia-ide/vscode-builtin-extensions/releases/download/v1.39.1-prel/log-1.39.1-prel.vsix";
      "vscode-builtin-markdown"= "https://github.com/theia-ide/vscode-builtin-extensions/releases/download/v1.39.1-prel/markdown-1.39.1-prel.vsix";
      "vscode-builtin-markdown-language-features"= "https://github.com/theia-ide/vscode-builtin-extensions/releases/download/v1.39.1-prel/markdown-language-features-1.39.1-prel.vsix";
      "vscode-builtin-merge-conflicts"= "https://github.com/theia-ide/vscode-builtin-extensions/releases/download/v1.39.1-prel/merge-conflict-1.39.1-prel.vsix";
      "vscode-builtin-shellscript"= "https://github.com/theia-ide/vscode-builtin-extensions/releases/download/v1.39.1-prel/shellscript-1.39.1-prel.vsix";
      "vscode-builtin-theme-defaults"= "https://github.com/theia-ide/vscode-builtin-extensions/releases/download/v1.39.1-prel/theme-defaults-1.39.1-prel.vsix";
      "vscode-builtin-xml"= "https://github.com/theia-ide/vscode-builtin-extensions/releases/download/v1.39.1-prel/xml-1.39.1-prel.vsix";
      "vscode-builtin-yaml"= "https://github.com/theia-ide/vscode-builtin-extensions/releases/download/v1.39.1-prel/yaml-1.39.1-prel.vsix";
      "vscode-builtin-python"= "https://github.com/theia-ide/vscode-builtin-extensions/releases/download/v1.39.1-prel/python-1.39.1-prel.vsix";
      "vscode-editorconfig"= "https://github.com/theia-ide/editorconfig-vscode/releases/download/v0.14.4/EditorConfig-0.14.4.vsix";
      "ms-python.python"= "https://open-vsx.org/api/ms-python/python/2020.10.332292344/file/ms-python.python-2020.10.332292344.vsix";
      "vscode-python"= "https://github.com/microsoft/vscode-python/releases/download/2020.11.367453362/ms-python-release.vsix";
      "ms-pyright.pyright"= "https://open-vsx.org/api/ms-pyright/pyright/1.1.85/file/ms-pyright.pyright-1.1.85.vsix";
     };
     devDependencies = {
       "@theia/cli" = "latest";
     };
     resolutions ={};
     theiaPluginsDir = "plugins";
     scripts = {
        prepare = "yarn run clean && yarn build && yarn run download:plugin";
        clean= "theia clean";
        build= "theia build";
        start= "theia start --plugins=local-dir:plugins";
        "download:plugin" = "theia download:plugins";
      };
  };

  theiaidePackageFile = pkgs.writeText "package.json" (builtins.toJSON theiaidePackageConfig);

  base_service_file = pkgs.writeText "nixide.service" ''
    [Unit]
    Description=nixide service
    After=network.target
    StartLimitIntervalSec=0

    [Service]
    Type=simple
    Restart=always
    RestartSec=1
    Environment=THEIA_IDE_APP_FOLDER=THEIA_IDE_APP_FOLDER_PLACE_HOLDER
    Environment=PROJECT_FOLDER=PROJECT_FOLDER_PLACE_HOLDER
    WorkingDirectory=WORKING_DIRECTORY_PLACE_HOLDER
    ExecStart=/usr/bin/nix-shell --expr 'with import <nixpkgs> {}; callPackage PWD_PLACE_HOLDER/start-ide.nix {listenAddress = "${listenAddress}"; listenPort = ${toString listenPort}; theiaideAppFolder = "${theiaideAppFolder}"; projectFolder = "${projectFolder}";}' --pure --run run

    [Install]
    WantedBy=multi-user.target
  '';

  base_clean_file = pkgs.writeText "clean-nixide.service" ''
    [Unit]
    Description= nixide clean service

    [Service]
    Type=Oneshot
    Restart=always
    RestartSec=1
    Environment=THEIA_IDE_APP_FOLDER=THEIA_IDE_APP_FOLDER_PLACE_HOLDER
    WorkingDirectory=WORKING_DIRECTORY_PLACE_HOLDER
    ExecStart=rm -rf $THEIA_IDE_APP_FOLDER

    [Install]
    WantedBy=multi-user.target
  '';
in
pkgs.mkShell {
  name = "dev-shell";
  buildInputs = theiaideBuildDependencies ++ projectDependencies ++ theiaideOpenDependencies;
  shellHook =
  ''
    export THEIA_IDE_APP_FOLDER=$(realpath ${theiaideAppFolder})
    export PROJECT_FOLDER=$(realpath ${projectFolder})
    export WORKING_FOLDER=$(realpath ${workingFolder})

    function create_service() {
      export NIXIDE_SERVICE_NAME=''${1:-nixide}
      cp -f ${base_service_file} $NIXIDE_SERVICE_NAME.service
      sed -i "s|THEIA_IDE_APP_FOLDER_PLACE_HOLDER|$THEIA_IDE_APP_FOLDER|g" $NIXIDE_SERVICE_NAME.service
      sed -i "s|PROJECT_FOLDER_PLACE_HOLDER|$PROJECT_FOLDER|g" $NIXIDE_SERVICE_NAME.service
      sed -i "s|WORKING_DIRECTORY_PLACE_HOLDER|$WORKING_FOLDER|g" $NIXIDE_SERVICE_NAME.service
      sed -i "s|PWD_PLACE_HOLDER|$PWD|g" $NIXIDE_SERVICE_NAME.service

      cp -f ${base_clean_file} clean-$NIXIDE_SERVICE_NAME.service
      sed -i "s|THEIA_IDE_APP_FOLDER_PLACE_HOLDER|$THEIA_IDE_APP_FOLDER|g" clean-$NIXIDE_SERVICE_NAME.service
      sed -i "s|WORKING_DIRECTORY_PLACE_HOLDER|$WORKING_FOLDER|g" clean-$NIXIDE_SERVICE_NAME.service
    }

    function build() {
      mkdir -p $THEIA_IDE_APP_FOLDER && \
      rm -f $THEIA_IDE_APP_FOLDER/package.json && \
      ln -s ${theiaidePackageFile} $THEIA_IDE_APP_FOLDER/package.json && \
      cd $THEIA_IDE_APP_FOLDER && \
      yarn --ignore-engines
    }

    function start() {
      cd $THEIA_IDE_APP_FOLDER && \
      yarn start $PROJECT_FOLDER --hostname ${listenAddress} --port ${toString listenPort} --ignore-engines
    }

   function open() {
      start &
      sleep 5
      ${browser} http://${listenAddress}:${toString listenPort}
      wait
    }

    function clean() {
      rm -rf $THEIA_IDE_APP_FOLDER
    }
  '';
}
