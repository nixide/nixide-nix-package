# nixide-nix-package

link to repo: [https://gitlab.com/nixide/nixide-nix-package](https://gitlab.com/nixide/nixide-nix-package)

with the ide.nix file you can describe your theia ide and run and open it like

```sh
nix-shell --expr 'with import <nixpkgs> {}; callPackage ./ide.nix {}' --pure --run "build;open"
```

## Parameters

```nix
{
  pkgs ? import <nixpkgs> {},
  listenAddress ? "0.0.0.0",
  listenPort ? 3000,
  theiaideAppFolder ? "_theiaideApp",
  projectFolder ? "$PWD", # folder which contains the files, which should show inside the ide
  workingFolder ? "$PWD",
  browser ? "firefox" # use to open the ide site
}
```


## Commands

### build

build the ide:

```sh
nix-shell --expr 'with import <nixpkgs> {}; callPackage ./ide.nix {}' --pure --run "build"
```

### start

start the ide serivce:

```sh
nix-shell --expr 'with import <nixpkgs> {}; callPackage ./ide.nix {}' --pure --run "start"
```

### open

start the ide service and open the ide site in browser:

```sh
nix-shell --expr 'with import <nixpkgs> {}; callPackage ./ide.nix {}' --pure --run "open"
```

### clean

clean the ide (`theiaideAppFolder`):

```sh
nix-shell --expr 'with import <nixpkgs> {}; callPackage ./ide.nix {}' --pure --run "clean"
```

### create_service

createa systemd service file for the ide:

```sh
# <ide_name> is a optional parameter. Default value is nixide
nix-shell --expr 'with import <nixpkgs> {}; callPackage ./ide.nix {}' --pure --run "create_service [<ide_name>:nixde]"
```

this creates two files:
* `<ide_name>.service` - a service which start the ide service
* `clean-<ide_name>.service` - a service which clean the ide (`theiaideAppFolder`)

Copy theese files to your systemd service folder like `/etc/systemd/system/` or `~/.config/systemd/user/`.  Then you can start your ide with `systemd start <ide_name>` or `systemd start clean-<ide_name>` to clean the it.


## Config

### Project Dependencies

```nix
projectDependencies = [
  # list of packages which are required by the project which  the ide should show
];
```

### Ide Config

```nix
  theiaidePackageConfig = {
     private= true;
     theia.frontend.config.applicationName = "NixIde-Package";
     dependencies = {
       # list of theia components (version) e.g. "@theia/terminal"= "latest";
     }:
     theiaPlugins = { 
       # list of theia plugin e.g."vscode-builtin-markdown"= "https://url.to.vsix;    
     };
     # ...
  }:
```

this will config object will be convert to the theia `package.json`.

